package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UtilisateurDetailsImpl  implements UserDetails {
    private static final long serialVersionUID = 1L;


    private String id;

    private String username;

    private String email;

    @JsonIgnore
    private String password;
    private  String adresse;
    private String date_naiss;

    private Long phone;

    private Collection<? extends GrantedAuthority> authorities;

    public UtilisateurDetailsImpl(String id, String username, String email,
                                  String password,String adresse,
                                  Long phone, String date_naiss,
                                  Collection<? extends GrantedAuthority> authorities){
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.adresse = adresse;
        this.phone = phone;
        this.date_naiss = date_naiss;
        this.authorities = authorities;
    }
    public static UtilisateurDetailsImpl build(Utilisateur user) {

        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority
                        (role.getName().name()))
                .collect(Collectors.toList());
        return new UtilisateurDetailsImpl(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getAdresse(),
                user.getPhone(),
                user.getDate_naiss(),
                authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
    public String getId() {
        return id;
    }
public String getAdresse(){
        return adresse;
}
    public String getEmail() {
        return email;
    }


    @Override
    public String getPassword() {
        return password;
    }


    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    // mana3resh shtaamel nsit
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UtilisateurDetailsImpl user = (UtilisateurDetailsImpl) o;
        return Objects.equals(id, user.id);
    }
}
