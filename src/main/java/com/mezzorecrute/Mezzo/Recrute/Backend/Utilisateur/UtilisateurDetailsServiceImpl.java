package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UtilisateurDetailsServiceImpl  implements UserDetailsService {
    @Autowired
    UtilisateurRepository utilisateurRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Utilisateur> utilisateur = utilisateurRepository.findByMatriculerhOrEmail(username,username);
        if(!utilisateur.isPresent())
            throw new UsernameNotFoundException(" not exists");
        return UtilisateurDetailsImpl.build(utilisateur.get());
    }
}
