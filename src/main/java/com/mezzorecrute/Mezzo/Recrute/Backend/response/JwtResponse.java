package com.mezzorecrute.Mezzo.Recrute.Backend.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private String id;
    private String username;
    private String email;
    private String password;
    private String adresse;
    private Long phone;
    private String date_naiss;

    private List<String> roles;


    public JwtResponse(String accessToken, String id, String username,  String email,  String password, String adresse,Long phone ,String date_naiss, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.password=password;
        this.adresse=adresse;
        this.phone=phone;
        this.date_naiss=date_naiss;
        this.roles = roles;
    }
}
