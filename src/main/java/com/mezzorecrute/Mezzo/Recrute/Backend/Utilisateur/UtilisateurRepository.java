package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UtilisateurRepository
        extends MongoRepository<Utilisateur, String> {
    // pour tester si le email eli besh yet3mal jdid mawjoud w ela le

    Boolean existsByEmail(String email);
    // pour tester si le matricule eli besh yet3mal jdid mawjoud w ela le
    Boolean existsByMatriculerh(String matriculerh);
    // hedhy l fazet login b email w ela matricule
    Optional<Utilisateur> findByMatriculerhOrEmail(String matriculerh, String email);
}
